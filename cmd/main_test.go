package main

import (
	"fmt"
	"strconv"
	"strings"
	"testing"
	"time"
)

func TestFetchPage(t *testing.T) {
	fmt.Println("TestFetchPage")
	res, err := fetchWebpage("https://www.procyclingstats.com/race/tour-de-france/2022/gc")
	if err != nil {
		fmt.Printf("Error fetching page %v\n", err)
	}
	fmt.Println(*res)
}

func TestGetResultsStage1(t *testing.T) {
	fmt.Println("Get results called")

	res, _ := fetchWebpage("https://www.procyclingstats.com/race/tour-de-france/2023")
	firstStageStage2023 := time.Date(2023, 7, 1, 20, 0, 0, 0, time.Local)
	urlExtension, _ := deriveStage(res, firstStageStage2023)
	fmt.Println("urlExtension", urlExtension)

	splitOnHyphon := strings.Split(urlExtension, "-")
	stageNum, _ := strconv.Atoi(splitOnHyphon[len(splitOnHyphon)-1])
	fmt.Println("stageNum", stageNum)
	res, _ = fetchWebpage(fmt.Sprintf("https://www.procyclingstats.com/%s", urlExtension))
	res2, _ := GetResults(res, stageNum)
	fmt.Println(res2)
}

func TestGetResultsToday(t *testing.T) {
	fmt.Println("Get results called")

	res, _ := fetchWebpage("https://www.procyclingstats.com/race/tour-de-france/2024")
	firstStageStage2023 := time.Now()
	urlExtension, _ := deriveStage(res, firstStageStage2023)
	fmt.Println("urlExtension", urlExtension)

	splitOnHyphon := strings.Split(urlExtension, "-")
	stageNum, _ := strconv.Atoi(splitOnHyphon[len(splitOnHyphon)-1])
	fmt.Println("stageNum", stageNum)
	res, _ = fetchWebpage(fmt.Sprintf("https://www.procyclingstats.com/%s", urlExtension))
	_, err := GetResults(res, stageNum)
	fmt.Println(err)
}
func TestGetResultsStage4(t *testing.T) {
	fmt.Println("Get results called")

	res, _ := fetchWebpage("https://www.procyclingstats.com/race/tour-de-france/2023")
	fourthStageStage2023 := time.Date(2023, 7, 4, 20, 0, 0, 0, time.Local)
	urlExtension, _ := deriveStage(res, fourthStageStage2023)

	splitOnHyphon := strings.Split(urlExtension, "-")
	stageNum, _ := strconv.Atoi(splitOnHyphon[len(splitOnHyphon)-1])
	fmt.Println("stageNum", stageNum)
	res, _ = fetchWebpage(fmt.Sprintf("https://www.procyclingstats.com/%s", urlExtension))
	res2, _ := GetResults(res, stageNum)
	fmt.Println(res2)
}

func TestGetResultsJointLeaders(t *testing.T) {
	fmt.Println("Get results called")

	res, _ := fetchWebpage("https://www.procyclingstats.com/race/tour-de-france/2024")
	secondStageStage2024 := time.Date(2024, 6, 30, 20, 0, 0, 0, time.Local)
	urlExtension, _ := deriveStage(res, secondStageStage2024)

	splitOnHyphon := strings.Split(urlExtension, "-")
	stageNum, _ := strconv.Atoi(splitOnHyphon[len(splitOnHyphon)-1])
	fmt.Println("stageNum", stageNum)
	res, _ = fetchWebpage(fmt.Sprintf("https://www.procyclingstats.com/%s", urlExtension))
	res2, _ := GetResults(res, stageNum)
	fmt.Println(res2["VINGEGAARD Jonas"])

}
/*
func TestDeriveStage(t *testing.T) {

	res, _ := fetchWebpage("https://www.procyclingstats.com/race/tour-de-france/2023")
	fmt.Println(deriveStage(res, time.Now()))
}
*/

/*
func TestGetLeaderTimeInSeconds(t *testing.T) {
	fmt.Println("Get results called")

	res, _ := fetchWebpage("https://www.procyclingstats.com/race/tour-de-france/2022/gc")
	comp, _ := GetResults(res)

	t1, err := getLeaderTime(comp)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(t1)


}
*/

/*
func TestGetLastTimeInSeconds(t *testing.T) {
	fmt.Println("Get results called")

	res, _ := fetchWebpage("https://www.procyclingstats.com/race/tour-de-france/2022/gc")
	comp, _ := GetResults(res)

	t1, err := getLeaderTime(comp)
	if err != nil {
		fmt.Println(err)
	}

	t2, err := getLastRiderTime(comp, t1)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(t2)

}*/

/*
func TestConvertToSeconds(t *testing.T) {
	t1 := "2:43"
	t2, err := convertTimeToSeconds(t1)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(t2)
}


*/
