package main

import (
	"encoding/json"
	"fmt"
	"io"
	"math"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"golang.org/x/net/html"
)

type Result struct {
	Time              string
	TimeRanking       int `json:"time_ranking"`
	SprintRanking     int `json:"sprint_ranking"`
	YoungRiderRanking int `json:"young_rider_ranking"`
	ClimberRanking    int `json:"climber_ranking"`
	Sprint            int `json:"sprint"`
	Climber           int `json:"climber"`
}

type rider struct {
	Name              string
	ID                string
	TotalTime         string
	TotalTimeSeconds  int
	Time              string
	TimeRanking       int
	Sprint            int
	SprintRanking     int
	Climber           int
	ClimberRanking    int
	YoungRiderRanking int
	Percentage        int
	DNF               bool
	Jerseys           []string
}

type ashmore struct {
	Name             string
	ID               string
	GraphColour      string
	TotalTime        string
	TotalTimeSeconds int
	Time             string
	Percentage       int
	Jerseys          []string
	Riders           []rider
}

type results struct {
	Results *[6]ashmore
}

const baseURL string = "https://www.procyclingstats.com"

func createTeam(teamName string, colour string, riderNames [8]string) (a ashmore) {
	riders := make([]rider, 0, 8)

	for _, i := range riderNames {
		r := rider{
			Name:    i,
			Jerseys: make([]string, 0, 4),
		}
		riders = append(riders, r)
	}
	a = ashmore{
		Name:        teamName,
		GraphColour: colour,
		Jerseys:     make([]string, 4),
		Riders:      riders,
	}
	return
}
func createAshmoresStruct() [6]ashmore {
	var ashmores [6]ashmore
	// Teams and colours

	// The boys #FFA500
	ashmores[0] = createTeam("Les Bebes Sauvages", "#FFA500", [8]string{"ROGLIČ Primož", "VINGEGAARD Jonas", "CAPIOT Amaury", "GHYS Robbe", "COSTA Rui", "GESCHKE Simon", "GALL Felix", "BURGAUDEAU Mathieu"})
	// Joe green
	ashmores[1] = createTeam("Uno-X Lacking Mobility", "green", [8]string{"AYUSO Juan", "POGAČAR Tadej", "GRIGNARD Sébastien", "LEMMEN Bart", "BERNAL Egan", "WELTEN Bram", "IZAGIRRE Ion", "BEULLENS Cedric"})
	// Freya #ff0
	ashmores[2] = createTeam("Jumbo On Ya Bike", "#ff0", [8]string{"YATES Adam", "PEDERSEN Mads", "LAPORTE Christophe", "MEZGEC Luka", "ONLEY Oscar", "NEILANDS Krists", "LAURANCE Axel", "VAN WILDER Ilan"})
	// Josh #c24646
	ashmores[3] = createTeam("Red Bull gives you wheels!", "#c24646", [8]string{"VAN DER POEL Mathieu", "EVENEPOEL Remco", "VERMEERSCH Gianni", "KULSET Johannes", "JEGAT Jordan", "LAPEIRA Paul", "CRAS Steff", "DRIZNERS Jarrad"})
	// Alison #005EB8
	ashmores[4] = createTeam("TrekSea Bright Futures", "#005EB8", [8]string{"PHILIPSEN Jasper", "DE LIE Arnaud", "EEKHOFF Nils", "THOMAS Geraint", "VAUQUELIN Kévin", "MÜHLBERGER Gregor", "PIDCOCK Thomas", "ALLEGAERT Piet"})
	// Richard purple
	ashmores[5] = createTeam("Uphill is easy with EPO", "purple", [8]string{"VLASOV Aleksandr", "VAN AERT Wout", "JAKOBSEN Fabio", "LAMPAERT Yves", "DUJARDIN Sandy", "LUTSENKO Alexey", "HALLER Marco", "JORGENSON Matteo"})
	return ashmores
}

func convertTimeToSeconds(time string) (int, error) {
	var h, m, s int
	var err error
	numOfColons := strings.Count(time, ":")
	if numOfColons == 0 {
		_, err = fmt.Sscanf(time, "%d", &s)
	} else if numOfColons == 1 {
		_, err = fmt.Sscanf(time, "%d:%d", &m, &s)
	} else if numOfColons == 2 {
		_, err = fmt.Sscanf(time, "%d:%d:%d", &h, &m, &s)
	}
	if err != nil {
		return 0, err
	}
	return int(h*3600 + m*60 + s), nil
}

func getLeaderTime(comp map[string]*Result) (int, error) {
	var leaderTimeInSeconds int
	var err error
	for _, v := range comp {
		if v.TimeRanking == 1 {
			// fmt.Println(i.Result.Time)
			leaderTimeInSeconds, err = convertTimeToSeconds(v.Time)
			if err != nil {
				return 0, err
			}
			break
		}
	}
	return leaderTimeInSeconds, nil
}

func getLastRiderTime(comp map[string]*Result, leaderTimeInSeconds int) (int, error) {
	var seconds int
	var err error
	var slowestRider string
	for k, v := range comp {
		if slowestRider == "" {
			slowestRider = k
		}
		if v.TimeRanking >= comp[slowestRider].TimeRanking {
			slowestRider = k
		}
	}
	if slowestRider != "" {
		seconds, err = convertTimeToSeconds(comp[slowestRider].Time)
		if err != nil {
			fmt.Println(err)
		}
	}
	return leaderTimeInSeconds + seconds, nil
}

func convertSecondsToFormattedTime(time int) string {
	var hours, minutes, seconds int
	hours = time / 3600
	remainder := time % 3600
	minutes = remainder / 60
	seconds = remainder % 60
	return fmt.Sprintf("%02d:%02d:%02d\n", hours, minutes, seconds)
}

func main() {
	lambda.Start(handler)
}

func calculatePercentage(time int, min int, diff int) int {
	return int((float32(time) - float32(min)) / float32(diff) * 100)
}

func fetchWebpage(url string) (page *string, err error) {
	resp, err := http.Get(url)
	if err != nil {
		fmt.Printf("Error fetching page %v\n", err)
		return
	}

	defer resp.Body.Close()

	br, err := io.ReadAll(resp.Body)
	if err != nil {
		fmt.Printf("Error getting body %v\n", err)
		return
	}

	str := string(br)
	page = &str
	return
}

func deriveStage(page *string, date time.Time) (stage string, stageData time.Time) {
	stageData = date
	doc, err := html.Parse(strings.NewReader(*page))
	if err != nil {
		fmt.Printf("Unable to parse html %s\n", err)
	}
	var f func(n *html.Node)
	found := false
	f = func(n *html.Node) {
		if found {
			return
		}
		if n.Data == fmt.Sprintf("%02d/%02d", date.Day(), date.Month()) {
			fmt.Println("found date")
			stageNode := n.Parent.NextSibling.NextSibling.NextSibling.FirstChild
			if stageNode.Data == "Restday" {
				stageNode = stageNode.Parent.Parent.PrevSibling.PrevSibling.FirstChild.NextSibling.NextSibling.FirstChild
				stageData = stageData.AddDate(0, 0, -1)
			}
			for _, v := range stageNode.Attr {
				if v.Key == "href" {
					stage = v.Val
					found = true
					break
				}
			}
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			f(c)
		}
	}
	f(doc)
	return
}

func isGeneralTable(n *html.Node) (result bool) {
	for _, v := range n.Parent.Attr {
		if v.Key == "data-subtab" && v.Val == "1" {
			result = true
			break
		}
	}
	return
}

func GetResults(page *string, stageNum int) (comp map[string]*Result, err error) {
	comp = make(map[string]*Result)
	const (
		GC     int = 2
		Points int = 3
		KOM    int = 4
		Youth  int = 5
	)

	doc, err := html.Parse(strings.NewReader(*page))
	if err != nil {
		fmt.Printf("Unable to parse html %s\n", err)
	}

	// I'll assume the tables are always ordered Stage - GC - Points - KOM - Youth - Team
	// Only interested in 2, 3, 4, 5
	// If the table order changes, to work out tables programmatically, need to parse the tabs (class retabs),
	// match the table name (e.g. GC) with the data-id (e.g. 1234) then use a map as a lookup when parsing the tables, as the divs wrapping the tables have this data-id as an attribute

	table := 0
	lastValidTime := ""
	leaderTime := ""
	var f func(n *html.Node)
	f = func(n *html.Node) {
		if n.Type == html.ElementNode && n.Data == "table" && isGeneralTable(n) {
			table++
		} else if n.Type == html.ElementNode && n.Data == "table" {
			// skip
			return
		}

		if n.Type == html.ElementNode && n.Data == "tr" && n.Parent.Data == "tbody" && (table == GC || table == Points || table == KOM || table == Youth) {
			// for stage one
			var rider string
			if stageNum == 1 {
				rider = n.FirstChild.NextSibling.NextSibling.NextSibling.NextSibling.FirstChild.FirstChild.NextSibling.NextSibling.FirstChild.Data
			} else {
				rider = n.FirstChild.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.FirstChild.FirstChild.NextSibling.NextSibling.FirstChild.Data
			}

			if comp[rider] == nil {
				comp[rider] = &Result{}
			}
			rank, _ := strconv.Atoi(n.FirstChild.FirstChild.Data)
			switch table {
			case GC:
				// for stage one
				var timeNode *html.Node
				if stageNum == 1 {
					timeNode = n.FirstChild.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.FirstChild
				} else if stageNum == 21 {
					timeNode = n.FirstChild.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.FirstChild
				} else {
					timeNode = n.FirstChild.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.FirstChild
				}
				if timeNode.Data == "span" {
					timeNode = timeNode.FirstChild
				}
				t := timeNode.Data
				if t == ",," && lastValidTime == leaderTime {
					t = "0:00"
				} else if t == ",," {
					t = lastValidTime
				} else {
					lastValidTime = t
					if n.FirstChild.FirstChild.Data == "1" {
						leaderTime = t
					}
				}
				comp[rider].Time = strings.TrimSpace(t)
				comp[rider].TimeRanking = rank
			case Points:
				// For stage one
				var points int
				var pointsNode *html.Node
				if stageNum == 1 {
					pointsNode = n.FirstChild.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.FirstChild.FirstChild
				} else {
					pointsNode = n.FirstChild.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.FirstChild.FirstChild
				}
				if pointsNode == nil {
					points = 0
				} else {
					points, _ = strconv.Atoi(pointsNode.Data)
				}
				comp[rider].Sprint = points
				comp[rider].SprintRanking = rank
			case KOM:
				// for stage one
				var pointsStr string
				if stageNum == 1 {
					pointsStr = n.FirstChild.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.FirstChild.Data
				} else {
					pointsStr = n.FirstChild.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.NextSibling.FirstChild.Data
				}
				points, _ := strconv.Atoi(pointsStr)
				comp[rider].Climber = points
				comp[rider].ClimberRanking = rank
			case Youth:
				rank, _ := strconv.Atoi(n.FirstChild.FirstChild.Data)
				comp[rider].YoungRiderRanking = rank
				comp[rider].YoungRiderRanking = rank
			}
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			f(c)
		}
	}
	f(doc)
	return
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	ashmores := createAshmoresStruct()
	fmt.Println("Handler starts")

	date := time.Now()
	if date.Hour() < 17 {
		date = time.Now().AddDate(0, 0, -1)
	}

	resp, err := fetchWebpage(fmt.Sprintf("%s/race/tour-de-france/%d", baseURL, date.Year()))
	if err != nil {
		fmt.Printf("Error fetching summary page %v\n", err)
	}

	urlExtension, _ := deriveStage(resp, date)

	resp, err = fetchWebpage(fmt.Sprintf("%s/%s/", baseURL, urlExtension))
	if err != nil {
		fmt.Printf("Error fetching PCS page %v\n", err)
	}
	splitOnHyphon := strings.Split(urlExtension, "-")
	stageNum, _ := strconv.Atoi(splitOnHyphon[len(splitOnHyphon)-1])

	comp, err := GetResults(resp, stageNum)
	if err != nil {
		fmt.Printf("Error parsing results %v\n", err)
	}

	// Get time of 1st rider
	leaderTimeInSeconds, err := getLeaderTime(comp)
	if err != nil {
		fmt.Println("Problem retriving leader's time", err)
	}

	// fmt.Println(leaderTimeInSeconds)
	//Get time of last rider
	slowestTimeInSeconds, err := getLastRiderTime(comp, leaderTimeInSeconds)
	if err != nil {
		fmt.Println("Problem retiving last rider's time", err)
	}

	var maxAshmoreTime, maxRiderTime, minAshmoreTime, minRiderTime, ashmoreTimeDiff, riderTimeDiff, yellowJersey, greenJersey, polkaJersey, whiteJersey int
	var yellowJerseyRider, greenJerseyRider, whiteJerseyRider, polkaJerseyRider string
	minAshmoreTime = math.MaxInt
	minAshmoreTime = math.MaxInt
	minRiderTime = math.MaxInt
	yellowJersey = math.MaxInt
	greenJersey = math.MaxInt
	polkaJersey = math.MaxInt
	whiteJersey = math.MaxInt

	// populate struct with times from api request

	var seconds int
	for i := 0; i < len(ashmores); i++ {
		for j := 0; j < len(ashmores[i].Riders); j++ {
			if comp[ashmores[i].Riders[j].Name] == nil {
				seconds += slowestTimeInSeconds
				ashmores[i].Riders[j].DNF = true
				ashmores[i].TotalTimeSeconds += seconds
				ashmores[i].Riders[j].TotalTimeSeconds = seconds
				ashmores[i].Riders[j].TotalTime = convertSecondsToFormattedTime(seconds)
				ashmores[i].Riders[j].Time = fmt.Sprintf("+%v", convertSecondsToFormattedTime(seconds-leaderTimeInSeconds))
			}
			seconds = 0
		}
	}
	for i := 0; i < len(ashmores); i++ {
		for j := 0; j < len(ashmores[i].Riders); j++ {
			// find rider from api
			for k, v := range comp {
				if ashmores[i].Riders[j].Name == k {
					if v.TimeRanking == 1 {
						seconds = leaderTimeInSeconds
					} else {
						seconds, err = convertTimeToSeconds(v.Time)
						if err != nil {
							fmt.Println("Issue converting time for rider", ashmores[i].Riders[j].Name, err)
						}
						seconds += leaderTimeInSeconds
					}
					ashmores[i].TotalTimeSeconds += seconds
					ashmores[i].Riders[j].TotalTimeSeconds = seconds
					ashmores[i].Riders[j].TotalTime = convertSecondsToFormattedTime(seconds)
					ashmores[i].Riders[j].Time = fmt.Sprintf("+%v", convertSecondsToFormattedTime(seconds-leaderTimeInSeconds))

					ashmores[i].Riders[j].TimeRanking = v.TimeRanking
					ashmores[i].Riders[j].Sprint = v.Sprint
					ashmores[i].Riders[j].SprintRanking = v.SprintRanking
					ashmores[i].Riders[j].Climber = v.Climber
					ashmores[i].Riders[j].ClimberRanking = v.ClimberRanking
					ashmores[i].Riders[j].YoungRiderRanking = v.YoungRiderRanking

					seconds = 0

					if v.TimeRanking != 0 && v.TimeRanking < yellowJersey {
						yellowJersey = v.TimeRanking
						yellowJerseyRider = ashmores[i].Riders[j].Name
					}

					if v.SprintRanking != 0 && v.SprintRanking < greenJersey {
						greenJersey = v.SprintRanking
						greenJerseyRider = ashmores[i].Riders[j].Name
					}

					if v.ClimberRanking != 0 && v.ClimberRanking < polkaJersey {
						polkaJersey = v.ClimberRanking
						polkaJerseyRider = ashmores[i].Riders[j].Name
					}

					if v.YoungRiderRanking != 0 && v.YoungRiderRanking < whiteJersey {
						whiteJersey = v.YoungRiderRanking
						whiteJerseyRider = ashmores[i].Riders[j].Name
					}
					break
				}
			}
		}
		ashmores[i].TotalTime = convertSecondsToFormattedTime(ashmores[i].TotalTimeSeconds)
	}

	// find min and maxs

	for i := 0; i < len(ashmores); i++ {
		if ashmores[i].TotalTimeSeconds > maxAshmoreTime {
			maxAshmoreTime = ashmores[i].TotalTimeSeconds
		}

		if ashmores[i].TotalTimeSeconds < minAshmoreTime {
			minAshmoreTime = ashmores[i].TotalTimeSeconds
		}

		for j := 0; j < len(ashmores[i].Riders); j++ {
			if ashmores[i].Riders[j].TotalTimeSeconds > maxRiderTime {
				maxRiderTime = ashmores[i].Riders[j].TotalTimeSeconds
			}

			if ashmores[i].Riders[j].TotalTimeSeconds < minRiderTime {
				minRiderTime = ashmores[i].Riders[j].TotalTimeSeconds
			}
		}
	}

	// calculate diffs in min/max time

	ashmoreTimeDiff = int((float32(maxAshmoreTime) - float32(minAshmoreTime)) / 90 * 100)
	minAshmoreTimeSmallBar := maxAshmoreTime - ashmoreTimeDiff
	riderTimeDiff = int((float32(maxRiderTime) - float32(minRiderTime)) / 90 * 100)
	minRiderTime = maxRiderTime - riderTimeDiff

	var tempAsh ashmore
	var tempRider rider

	// sort in ascending order

	for i := 0; i < (len(ashmores) - 1); i++ {
		for j := i + 1; j < len(ashmores); j++ {
			if ashmores[i].TotalTimeSeconds > ashmores[j].TotalTimeSeconds {
				tempAsh = ashmores[i]
				ashmores[i] = ashmores[j]
				ashmores[j] = tempAsh
			}

		}

	}
	for i := 0; i < len(ashmores); i++ {
		for j := 0; j < (len(ashmores[i].Riders) - 1); j++ {
			for k := j + 1; k < len(ashmores[i].Riders); k++ {
				if ashmores[i].Riders[j].TotalTimeSeconds > ashmores[i].Riders[k].TotalTimeSeconds {
					tempRider = ashmores[i].Riders[j]
					ashmores[i].Riders[j] = ashmores[i].Riders[k]
					ashmores[i].Riders[k] = tempRider
				}
			}
		}
	}

	// calculate percentage

	for i := 0; i < len(ashmores); i++ {
		if i == 0 {
			ashmores[i].Time = ashmores[i].TotalTime
		} else {
			ashmores[i].Time = fmt.Sprintf("+%v", convertSecondsToFormattedTime(ashmores[i].TotalTimeSeconds-minAshmoreTime))
		}
		ashmores[i].Percentage = calculatePercentage(ashmores[i].TotalTimeSeconds, minAshmoreTimeSmallBar, ashmoreTimeDiff)
		for j := 0; j < len(ashmores[i].Riders); j++ {
			ashmores[i].Riders[j].Percentage = calculatePercentage(ashmores[i].Riders[j].TotalTimeSeconds, minRiderTime, riderTimeDiff)
		}
	}

	// calculate jerseys
	for i := 0; i < len(ashmores); i++ {
		for j := 0; j < len(ashmores[i].Riders); j++ {
			if ashmores[i].Riders[j].Name == yellowJerseyRider {
				ashmores[i].Riders[j].Jerseys = append(ashmores[i].Riders[j].Jerseys, "gc")
				ashmores[i].Jerseys = append(ashmores[i].Jerseys, "gc")
			}

			if ashmores[i].Riders[j].Name == greenJerseyRider {
				ashmores[i].Riders[j].Jerseys = append(ashmores[i].Riders[j].Jerseys, "s")
				ashmores[i].Jerseys = append(ashmores[i].Jerseys, "s")
			}

			if ashmores[i].Riders[j].Name == polkaJerseyRider {
				ashmores[i].Riders[j].Jerseys = append(ashmores[i].Riders[j].Jerseys, "c")
				ashmores[i].Jerseys = append(ashmores[i].Jerseys, "c")
			}

			if ashmores[i].Riders[j].Name == whiteJerseyRider {
				ashmores[i].Riders[j].Jerseys = append(ashmores[i].Riders[j].Jerseys, "y")
				ashmores[i].Jerseys = append(ashmores[i].Jerseys, "y")
			}
		}
	}

	// set ID
	r := strings.NewReplacer(" ", "", ",", "", "?", "", "'", "", "!", "")
	for i := 0; i < len(ashmores); i++ {
		ashmores[i].ID = r.Replace(ashmores[i].Name)
		for j := 0; j < len(ashmores[i].Riders); j++ {
			ashmores[i].Riders[j].ID = r.Replace(ashmores[i].Riders[j].Name)
		}
	}
	res := &results{
		Results: &ashmores,
	}

	data, err := json.Marshal(res)
	if err != nil {
		fmt.Println("Error turning struct to json", err)
	}
	return events.APIGatewayProxyResponse{
		Body:       fmt.Sprintf("%v\n", string(data)),
		StatusCode: 200,
	}, nil

}
